import listes.Cons
import listes.Liste
import listes.Nil
import option.Option


/*****************
 * PARTIE 1
 *****************/

/* Q1 - findFirst: recherche du 1er indice de chaÃ®ne de caractÃ¨re dans une liste */
fun findFirst(l : Liste<String>, s : String) : Int = TODO()

/* Q2 - findFirst: recherche du premier element d'une liste satisfaisant un prÃ©dicat donnÃ© */
// TODO

/* Q3 - append: concatÃ¨ne deux listes */
// TODO

/* Q4 - all: renvoie true tous les elements de l satisfont p */
fun <A> all(l : Liste<A>, p : (A) -> Boolean): Boolean  = TODO()

/* Q5 - flatMap: concatÃ¨ne les listes d'une liste de listes */
// TODO

/* Q6 - isSorted: renvoie true ssi la liste d'entiers est triÃ©e */
fun Liste<Int>.isSorted() : Boolean = TODO()

/* Q7 - data class Personne */
// TODO

/* Q8 - isSortedGeneric: renvoie true ssi la liste d'Ã©lÃ©ments Comparable est triÃ©e */
// TODO

/*****************
 * PARTIE 2
 *****************/

/* Q9 - map: applique une fonction f Ã  des valeurs optionnelles */
fun <A,B> Option<A>.map(f:(A)->B): Option<B> = TODO()

/* Q10 - incOption: incrÃ©mente un entier optionnel */
fun incOption(a : Option<Int>) : Option<Int> = TODO()

// Q11 - findFirst dont le traitement des exceptions est rÃ©alisÃ© avec Option
fun <A> findFirstOption(l: Liste<A>, p: (A) -> Boolean) : Option<Int> = TODO()

// Q12 - findFirst rÃ©cursif terminal et traitement des exceptions avec Option
fun <A> findFirstOption2(l: Liste<A>, p: (A) -> Boolean) : Option<Int> = TODO()

data class Employe(val nom: String, val departement: String, val manager: Option<Employe>)

/* Q13 - lookupByName : renvoie le l'employe ayant le nom donnÃ© en paramÃ¨tre */
fun lookupByName(employes : Liste<Employe>, nom : String) : Option<Employe> = TODO()

/* Q14 - flatMap */
fun <A,B> Option<A>.flatMap(f : (A) -> Option<B>): Option<B> = TODO()

/* Q15 - Salarie, Chef et Collaborateur */
// TODO

/* Q16 - fibonacci */
fun fibonacci(n : Int) : Long = TODO()

/* Q17 - fibonacci rÃ©cursive terminale */
fun fibonacciTerminal(n : Int) : Long = TODO()

fun main() {
}