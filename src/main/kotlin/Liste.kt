package listes

/* Listes */
sealed class Liste<out A>{
    companion object {
        /* Factory */
        fun <A> of(vararg xs : A) : Liste<A> =
            if (xs.isEmpty()) Nil
            else Cons(xs[0], of(*xs.sliceArray(1 until xs.size)))

        /* Somme d'entiers */
        fun sum(l : Liste<Int>) : Int = when (l) {
            is Nil -> 0
            is Cons -> l.head + sum(l.tail)
        }

        /* produit de doubles */
        fun product(l : Liste<Double>) : Double = when (l) {
            is Nil -> 1.0
            is Cons -> l.head * product(l.tail)
        }

        /* Destructurations: head et tail */
        fun <A> head(l : Liste<A>) : A = when (l) {
            is Nil -> throw IllegalStateException("head: Nil n'a pas de tÃªte")
            is Cons -> l.head
        }

        fun <A> tail(l : Liste<A>) : Liste<A> = when (l) {
            is Nil -> throw IllegalStateException("tail: Nil n'a pas de queue")
            is Cons -> l.tail
        }

        /* Remplace le premier Ã©lÃ©ment d'une liste par une autre valeur */
        fun <A> setHead(l : Liste<A>, x : A) : Liste<A> = when (l) {
            is Nil -> throw IllegalStateException("setHead: Nil n'a pas de tÃªte")
            is Cons -> Cons(x, tail(l))
        }

        /* Supprime les n premiers Ã©lÃ©ments d'une liste. */
        fun <A> drop(l : Liste<A>, n : Int) : Liste<A> = when {
            n<0 -> throw IllegalStateException("drop: n doit Ãªtre positif ou nul")
            n == 0 -> l
            else ->  when (l) {
                Nil -> throw IllegalStateException("drop: liste trop petite")
                else -> drop(tail(l), n-1)
            }
        }

        /* Supprime les premiers Ã©lÃ©ments d'une liste tant qu'ils satisfont une prÃ©dicat */
        fun <A> dropWhile(l: Liste<A>, p: (A) -> Boolean) : Liste<A> = when (l) {
            is Nil -> Nil
            is Cons -> if (p(l.head)) dropWhile(l.tail,p) else l
        }

        /* Longueur cumulÃ©e des chaÃ®nes de caractÃ¨res contenues dans une liste */
        fun lengthOfStrings(l : Liste<String>) : Int = when (l) {
            is Nil -> 0
            is Cons -> l.head.length + lengthOfStrings(l.tail)
        }

        /* foldRight : gÃ©nÃ©ralisation de sum, product */
        fun <A,B> foldRight(l : Liste<A>, z : B, f : (A,B) -> B ) : B = when (l) {
            is Nil -> z
            is Cons -> f(l.head, foldRight(l.tail,z,f))
        }

        /* RÃ©implÃ©mentation de sum, product et longueur */
        fun sum2(l : Liste<Int>) : Int = foldRight(l, 0, {a , b -> a+b} )
        fun product2(l : Liste<Double>) : Double = foldRight(l, 1.0, {a , b -> a*b} )
        fun lengthOfStrings2(l : Liste<String>) : Int = foldRight(l, 0, {a, b -> a.length + b})

        /* Longueur d'une liste avec foldRight */
        fun <A> length(l : Liste<A>) : Int = foldRight(l, 0, { a, b -> 1 + b})

        /* sum terminale */
        fun sumTerm(l : Liste<Int>) : Int {
            tailrec fun sumTermAux(l: Liste<Int>, acc: Int): Int = when (l) {
                is Nil -> acc
                is Cons -> sumTermAux(l.tail, l.head + acc)
            }
            return sumTermAux(l, 0)
        }

        /* product terminale */
        fun productTerm(l : Liste<Double>) : Double {
            tailrec fun productTermAux(l: Liste<Double>, acc: Double): Double = when (l) {
                is Nil -> acc
                is Cons -> productTermAux(l.tail, l.head + acc)
            }
            return productTermAux(l, 1.0)
        }

        /* lengthOfStrings terminale */
        fun lengthOfStringsTerm(l : Liste<String>) : Int {
            tailrec fun lengthOfStringsTermAux(l: Liste<String>, acc: Int): Int = when (l) {
                is Nil -> acc
                is Cons -> lengthOfStringsTermAux(l.tail, l.head.length + acc)
            }
            return lengthOfStringsTermAux(l, 1)
        }

        /* foldLeft */
        tailrec fun <A,B> foldLeft(l : Liste<A>, z : B, f : (B,A) -> B) : B = when (l) {
            is Nil -> z
            is Cons -> foldLeft(l.tail, f(z,l.head), f)
        }

        fun sumTerm2 (l : Liste<Int>) : Int = foldLeft(l, 0, { a, b -> a + b })
        fun productTerm2(l : Liste<Double>) : Double = foldLeft(l, 1.0, { a, b -> a * b })
        fun lengthOfStringsTerm2(l : Liste<String>) : Int = foldLeft(l, 1, { a, b -> a * b.length })
    }
}

object Nil : Liste<Nothing>()
data class Cons<out A>(val head : A, val tail : Liste<A>) : Liste<A>()
