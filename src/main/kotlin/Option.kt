package option

sealed class Option<out A>
data class Some<out A>(val x: A) : Option<A>()

object None : Option<Nothing>()

