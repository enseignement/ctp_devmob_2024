import listes.Liste
import listes.Nil
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class MainKtTest{
    /**********************
     * Q1 - findFirst: recherche du 1er indice de chaÃ®ne de caractÃ¨re dans une liste
     *********************** */

    @Test
    fun findFirst_existing_element_in_non_empty_list() {
        assertEquals(0,findFirst(Liste.of("1","2","3"),"1"))
        assertEquals(2,findFirst(Liste.of("1","2","3"),"3"))
    }

    @Test
    fun findFirst_absent_element_in_non_empty_list_should_raise_exception() {
        assertThrows<Exception> {
            findFirst(Liste.of("1","2","3"),"blop")
        }
    }
    @Test
    fun findFirst_element_in_empty_list_should_raise_exception(){
        assertThrows<Exception> {
            findFirst(Nil, "blop")
        }
    }

    /* ***************
     * Q2 - findFirst: recherche du premier element d'une liste satisfaisant un prÃ©dicat donnÃ©
     * *************** */

    /*
    @Test
    fun findFirstWithPredicate_existing_element_in_non_empty_list() {
        assertEquals(0,findFirst(Liste.of(1,2,3)) { it == 1})
        assertEquals(2,findFirst(Liste.of(1,2,3)) { it == 3})
    }

    @Test
    fun findFirstWithPredicate_absent_element_in_non_empty_list_should_raise_exception() {
        assertThrows<Exception> {
            findFirst(Liste.of(1,2,3)) { it < 0 }
        }
    }
    @Test
    fun findFirstWithPredicate_element_in_empty_list_should_raise_exception(){
        assertThrows<Exception> {
            findFirst(Nil as Liste<Int>) { it == 0 }
        }
    }
    */

    /* ***************
     * Q3 - append: concatÃ¨ne deux listes
     * *************** */

    /*
    @Test
    fun append_two_nil_listes_returns_nil_liste(){
        assertEquals(Nil, append(Nil,Nil))
    }

    @Test
    fun append_nil_with_non_nil_l_returns_l(){
        assertEquals(Liste.of(1,2), append(Nil,Liste.of(1,2)))
        assertEquals(Liste.of("1","2"), append(Nil,Liste.of("1","2")))
    }

    @Test
    fun append_non_nil_l_with_nil_returns_l(){
        assertEquals(Liste.of(1,2), append(Liste.of(1,2),Nil))
        assertEquals(Liste.of("1","2"), append(Liste.of("1","2"),Nil))
    }
    */


    /* ***************
     * Q4 - all: renvoie true si tous les elements de l statisfont p
     * *************** */

    /*
    @Test
    fun all_with_nil_list_returns_true(){
        assertTrue(all(Nil, { x -> true }))
    }

    @Test
    fun all_with_non_nil_list_and_some_element_not_satisfies_p_returns_false(){
        assertFalse(all(Liste.of(1,2,3), { x -> x<2 }))
        assertFalse(all(Liste.of(1,2,3), { x -> x>2 }))
    }

    @Test
    fun any_with_non_nil_list_and_all_element_satisfy_p_returns_true(){
        assertTrue(all(Liste.of(1,2,3), { x -> x<4 }))
    }
    */

    /* ***************
     * Q5 - flatMap: concatÃ¨ne les listes d'une liste de listes
     * *************** */

    /*
    @Test
    fun flatMap_of_nil_returns_nil(){
        assertEquals(Nil, flatMap(Nil as Liste<Liste<Any>>))
    }

    @Test
    fun flatMap_of_lists_returns_concatenation(){
        assertEquals(Liste.of(1,2,3,4,5,6), flatMap(Liste.of(Liste.of(1,2), Nil, Liste.of(3,4), Liste.of(5,6))))
    }
    */

    /* ***************
     * Q6 - isSorted: renvoie true ssi la liste d'entiers est triÃ©e
     * *************** */

    /*
    @Test
    fun isSorted_empty_list_returns_true(){
        assertTrue(Nil.isSorted())
    }

    @Test
    fun isSorted_sorted_list_returns_true(){
        assertTrue(Liste.of(1,5,13).isSorted())
        assertTrue(Liste.of(1,1).isSorted())
        assertTrue(Liste.of(1).isSorted())
    }

    @Test
    fun isSorted_non_sorted_list_returns_false(){
        assertFalse(Liste.of(5,7,1).isSorted())
        assertFalse(Liste.of(1,1,0).isSorted())
    }
    */

    /* ***************
     * Q7 - classe Personne
     * *************** */

    /*
    @Test
    fun Bob_is_younger_than_alice(){
        val bob = Personne("Bob", "Dylan", 15)
        val alice = Personne("Alice", "Cooper", 21)
        assertTrue(bob.compareTo(alice)<0)
    }

    @Test
    fun Alice_is_older_than_bob(){
        val bob = Personne("Bob", "Dylan", 15)
        val alice = Personne("Alice", "Cooper", 21)
        assertTrue(alice.compareTo(bob)>0)
    }

    @Test
    fun same_age_are_equal(){
        assertTrue(Personne("Bob", "Dylan", 15).compareTo(Personne("Mick", "Jagger", 15))==0)
    }
    */

    /* ***************
     * Q8 - isSortedGeneric: renvoie true ssi la liste d'Ã©lÃ©ments Comparable est triÃ©e
     * *************** */

    /*
    @Test
    fun isSortedGeneric_empty_list_returns_true(){
        val l : Liste<Personne> = Nil
        assertTrue(Nil.isSortedGeneric())
    }

    @Test
    fun isSortedGeneric_sorted_list_returns_true(){
        val listeDPersonnesTriee1 = Liste.of(
            Personne("robert", "aaa", 19),
            Personne("georges", "bbb", 21),
            Personne("louis", "ccc", 23))

        val listeDPersonnesTriee2 = Liste.of(
            Personne("robert", "aaa", 19),
            Personne("georges", "bbb", 19))

        assertTrue(listeDPersonnesTriee1.isSortedGeneric())
        assertTrue(listeDPersonnesTriee2.isSortedGeneric())
        assertTrue(Liste.of(Personne("robert", "aaa", 19)).isSortedGeneric())
    }

    @Test
    fun isSortedGeneric_non_sorted_list_returns_false(){
        val listeDePersonnesNonTriee = Liste.of(
            Personne("robert", "aaa", 19),
            Personne("louis", "ccc", 23),
            Personne("georges", "bbb", 21))

        assertFalse(listeDePersonnesNonTriee.isSortedGeneric())
    }

    */

    /*******************************
     * PARTIE 2
     *******************************/

    /* ***************
     * Q9 - map: applique une fonction f Ã  des valeurs optionnelles
     * *************** */

    /*
    @Test
    fun map_on_None_returns_None(){
        assertEquals(None,None.map({ x -> x }))
    }

    @Test
    fun map_on_Some_returns_Some(){
        assertEquals(Some(6),Some("Kotlin").map({s:String -> s.length}))
        assertEquals(Some(28),Some(14).map({a -> 2*a}))
    }
    */

    /* ***************
     * Q10 - incOption: incrÃ©mente un entier optionnel
     * *************** */
    /*
    @Test
    fun incOption_on_None_returns_None(){
        assertEquals(None,incOption(None))
    }

    @Test
    fun incOption_on_Some_returns_next_Some(){
        assertEquals(Some(1),incOption(Some(0)))
        assertEquals(Some(2),incOption(Some(1)))
    }
    */

    /* ***************
     * Q11 - findFirst et traitement des exceptions avec Option
     * *************** */
    /*
    @Test
    fun findFirstOption_existing_element_in_non_empty_list() {
        assertEquals(Some(0),findFirstOption(Liste.of(1,2,3)) { it == 1})
        assertEquals(Some(2),findFirstOption(Liste.of(1,2,3)) { it == 3})
    }

    @Test
    fun findFirstOption_absent_element_in_non_empty_list_returns_None() {
        assertEquals(None, findFirstOption(Liste.of(1,2,3)) { it < 0 })
    }
    @Test
    fun findFirstOption_element_in_empty_list_returns_None(){
        assertEquals(None, findFirstOption(Nil as Liste<Int>) { it == 0 })
    }
     */

    /* ***************
     * Q12 - findFirst rÃ©cursif terminal et traitement des exceptions avec Option
    * *************** */

    /*
    @Test
    fun findFirstOption2_existing_element_in_non_empty_list() {
        assertEquals(Some(0),findFirstOption2(Liste.of(1,2,3)) { it == 1})
        assertEquals(Some(2),findFirstOption2(Liste.of(1,2,3)) { it == 3})
    }

    @Test
    fun findFirstOption2_absent_element_in_non_empty_list_returns_None() {
        assertEquals(None, findFirstOption2(Liste.of(1,2,3)) { it < 0 })
    }

    @Test
    fun findFirstOption2_element_in_empty_list_returns_None(){
        assertEquals(None, findFirstOption2(Nil as Liste<Int>) { it == 0 })
    }
    */

    /* ***************
     * Q13 - lookupByName : renvoie le l'employe ayant le nom donnÃ© en paramÃ¨tre
     * *************** */

    /*
    val sj = Employe("Steve Jobs", "Commercial", None)
    val sw = Employe("Steve Wozniak", "DÃ©veloppement", None)
    val rw = Employe("Ronald Wayne", "Logistique", None)
    val bob = Employe("Bob", "Commercial", Some(sj))

    val lesEmployes = Liste.of(sj,sw,rw,bob,
        Employe("Boby", "Commercial", Some(bob)),
        Employe("Alice", "Commercial", Some(sj)),
        Employe("Robert", "DÃ©veloppement", Some(sw)),
        Employe("Barbara", "DÃ©veloppement", Some(sw)),
        Employe("Louise", "Logistique", Some(rw))
    )

    @Test
    fun lookupByName_on_empty_list_returns_None(){
    assertEquals(None,lookupByName(Nil,"GÃ©rard"))
    }

    @Test
    fun lookupByName_on_list_with_absent_name_returns_None(){
    assertEquals(None,lookupByName(lesEmployes,"GÃ©rard"))
    }

    @Test
    fun lookupByName_on_list_with_present_name_returns_person(){
    assertEquals(Some(bob),lookupByName(lesEmployes,"Bob"))
    }
    */

    /* ***************
     * Q14 - flatmap sur Option, test avec "manager"
     *     - manager : renvoie le manager d'un employÃ© donnÃ© s'il y en a un et None sinon
     * *************** */

    /*
    fun manager(employes : Liste<Employe>, nom : String) : Option<Employe> =
        lookupByName(employes,nom).flatMap { it.manager }

    @Test
    fun manager_on_empty_list_returns_None(){
    assertEquals(None,manager(Nil, "Bob"))
    }

    @Test
    fun manager_on_list_with_absent_employee_returns_None(){
    assertEquals(None,manager(lesEmployes, "Gerard"))
    }

    @Test
    fun manager_on_list_with_existing_employee_and_absent_manager_returns_None(){
    assertEquals(None,manager(lesEmployes, "Steve Jobs"))
    }

    @Test
    fun manager_on_list_with_existing_employee_and_manager_returns_manager(){
    assertEquals(Some(sj),manager(lesEmployes, "Bob"))
    }
    */

    /* ***************
     * Q15 - De meilleurs employes
    * *************** */

    /*
    val sj2 = Chef("Steve Jobs", "Commercial")
    val sw2 = Chef("Steve Wozniak", "DÃ©veloppement")
    val rw2 = Chef("Ronald Wayne", "Logistique")
    val bob2 = Collaborateur("Bob", sj2)

    val lesSalaries = Liste.of(sj2,sw2,rw2,bob2,
    Collaborateur("Boby", bob2),
    Collaborateur("Alice", sj2),
    Collaborateur("Robert", sw2),
    Collaborateur("Barbara", sw2),
    Collaborateur("Louise", rw2)
    )

    @Test
    fun collaborators_have_a_department() {
    assertEquals("Commercial",bob2.departement)
    assertEquals("Commercial",Collaborateur("Boby", bob2).departement)
    }

    @Test
    fun chiefs_have_no_manager(){
    assertEquals(None,sj2.manager)
    }
    */


    /* ***************
     * Q16 - Fibonacci
    * *************** */

    /*
    @Test
    fun fibo_0_returns_0(){
        assertEquals(0, fibonacci(0))
    }

    @Test
    fun fibo_1_returns_1(){
        assertEquals(1, fibonacci(1))
    }

    @Test
    fun fibo_recursive_cases(){
        assertEquals(13,fibonacci(7))
        assertEquals(34,fibonacci(9))
    }
    */

    /* ***************
     * Q16 - Fibonacci rÃ©cursif terminal
    * *************** */

    /*
    @Test
    fun fiboterm_0_returns_0(){
        assertEquals(0, fibonacciTerminal(0))
    }

    @Test
    fun fiboterm_1_returns_1(){
        assertEquals(1, fibonacciTerminal(1))
    }

    @Test
    fun fiboterm_recursive_cases(){
        assertEquals(13,fibonacciTerminal(7))
        assertEquals(34,fibonacciTerminal(9))
    }
    */
}
